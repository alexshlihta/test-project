<?php

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Auth::routes();

/*
|------------------------------------------------------------------------------------
| Admin
|------------------------------------------------------------------------------------
*/
Route::group(['prefix' => ADMIN, 'as' => ADMIN . '.', 'middleware'=>['auth', 'Role:10']], function () {
    Route::get('/', 'DashboardController@index')->name('dash');
    Route::resource('users', 'UserController');
    Route::resource('news', 'NewsController');
    
});

Route::get('/', function () {
    return view('pages.main');
});

Route::get('/all/news', 'PageController@index')->name('news.index');
Route::get('/aboutus', 'PageController@about')->name('about.us');
Route::get('/home', 'PageController@home')->name('home');
Route::get('/test', 'PageController@test')->name('test');
Route::get('/all/news/all', 'PageController@getNews');





<?php 

return [

	// My controllers
	"success_store"   => "Item added",
	"success_update"  => "Updated successfully",
	"success_destroy" => "Removed successfully",

	// My Views
	"manage"       => "Manage",
	"edit_title"   => "Edit Item",
	"delete_title" => "Delete Item",
	"add_new_item" => "New",
	"update_item" => "Update",
     "show_title"    => "Show Item",
    
    
    // View buttons
	"add_button"    => "Add",
	"edit_button"    => "Edit",
    "show_button"    => "Show",

	// Login, Logout ..etc
	"start_session"      => "Sign in to start your session",
	"remember_me"        => "Remember Me",
	"forgot_password"    => "I forgot my password",
	"login_btn"          => "Sign In",
	"reset_password"     => "Reset Password",
	"reset_password_btn" => "Send reset link",
	"connect"            => "Loggin",



];
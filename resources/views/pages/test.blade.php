<!doctype html>
<html>
<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TweenMax.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/scrollify/1.0.17/jquery.scrollify.min.js"></script>
    <script src="{{ asset('/js/my_js.js') }}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.min.css">
    <link rel="stylesheet" href="  https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.4.1/css/mdb.min.css">
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
</head>
<style>
    body {
        color: #282828;
        font-size: 20px;
    }
    body .slides__slide {
        background-color: #646464;
        display: flex;
        flex-direction: column;
        justify-content: space-around;
    }
    body .slides__slide h1 {
        font-size: 3rem;
        font-weight: 600;
        text-align: center;
    }

</style>
<body>
@include('blocks.navbar')
        @foreach($data as $news)
            <div class="slides">
            <section class="slides__slide" data-bg-color="rgba(100,100,100,1)">
            <div class="container">
                <div class="col-sm-12 mt-5 mb-5">
                    <div class="card-deck">
                        <div class="card">
                            <img class="card-img-top mx-auto" src="/images/500.png" alt="Card image cap" style="max-height: 40%; max-width: 50%">
                            <div class="card-body">
                                <h5 class="card-title">{!! $news->title !!}</h5>
                                <p class="card-text">{{substr("$news->description", 1, 150)}}{{"..."}}</p>
                                <p class="card-text text-right"><a href="self">{{"Read More"}}</a></p>

                            </div>
                            <div class="card-footer">
                                <small class="text-muted">{!! $news->publication_time !!}</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
            </div>
    @endforeach
            <div class="row">
                <div class="mx-auto" style="margin-top: 2%; margin-bottom: 2%">
                    {{ $data->links() }}
                </div>
            </div>
</body>
</html>
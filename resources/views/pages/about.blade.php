@extends('layouts.app_front')


<body class="page-template page-template-templates page-template-page-about page-template-templatespage-about-php page page-id-4604 desktop chrome" style="">
<div class="hero" style="background-image:url(../images/fon.jpg);"></div>
<main id="site-content" class="page about-page">

    <div class="wrap">
        <div class="text-max-wrap">
            <p>Founded in 2006, Substrakt work exclusively in the arts, cultural, heritage and creative sectors in Europe and North America. We specialise in producing smart, effective, beautiful and, above all, user-centred work.</p>
            <p>With offices in Birmingham’s Jewellery Quarter and Angel in north London we spend most of our time making websites and web applications. We also provide expert consultancy and training on analytics, ticketing, digital strategy and project management. Alongside these services we organise a regular, quarterly event, Digital Works, that brings together digital professionals from the sectors we work with to discuss, debate, explore and share experiences and expertise.</p>
            <p>We like to build long-term, strategic, successful partnerships with the organisations we work with.</p>
            <p>Interested in working with us? <a href="https://substrakt.com/get-in-touch/"><strong>Get in touch</strong></a> or <a href="https://substrakt.com/who-we-work-with/"><strong>take a look at some of the work we’ve done</strong></a>.</p>

        </div>

    </div>


    <section class="skills r-tabs" >

        <ul class="tab-set r-tabs-nav">
            <li class="r-tabs-tab r-tabs-state-active"><a href="#tab-dev" class="r-tabs-anchor">Web<br> Development</a></li>
            <li class="r-tabs-state-default r-tabs-tab"><a href="#tab-ux" class="r-tabs-anchor">User<br> Experience</a></li>
            <li class="r-tabs-state-default r-tabs-tab"><a href="#tab-ui" class="r-tabs-anchor">Interface<br> &amp; Interaction Design</a></li>
            <li class="r-tabs-state-default r-tabs-tab"><a href="#tab-apps" class="r-tabs-anchor">Mobile &amp; Large format<br> Applications</a></li>
            <li class="r-tabs-state-default r-tabs-tab"><a href="#tab-strategy" class="r-tabs-anchor">Strategy &amp;<br>Ongoing Support</a></li>
        </ul> <!-- skill-tabs-navigation -->

        <div class="r-tabs-accordion-title r-tabs-state-active"><a href="#tab-dev" class="r-tabs-anchor">Web<br> Development</a></div><div id="tab-dev" class="tab-content r-tabs-panel r-tabs-state-active" style="display: block;">
            <img src="https://d3oyac7tbvw6tq.cloudfront.net/2015/05/web-dev2-400x200.jpg" class=" lazyloaded" data-srcset="https://d3oyac7tbvw6tq.cloudfront.net/2015/05/web-dev2-1600x800.jpg 1w, https://d3oyac7tbvw6tq.cloudfront.net/2015/05/web-dev2-2000x1000.jpg 2w" srcset="https://d3oyac7tbvw6tq.cloudfront.net/2015/05/web-dev2-1600x800.jpg 1w, https://d3oyac7tbvw6tq.cloudfront.net/2015/05/web-dev2-2000x1000.jpg 2w">
            <div class="summary"><p>Whatever the requirements, we build usable and accessible websites and web applications. </p>
                <p>Our core strengths and expertise lie in WordPress and Ruby on Rails.</p>
                <p>We employ test-driven and behaviour-driven development techniques, build for scalability and contribute to the open source community.</p>
            </div>
        </div>
        <div class="r-tabs-accordion-title"><a href="#tab-ux" class="r-tabs-anchor">User<br> Experience</a></div><div id="tab-ux" class="tab-content r-tabs-state-default r-tabs-panel">
            <img src="https://d3oyac7tbvw6tq.cloudfront.net/2015/05/user_experience-400x200.jpg" class="lazyload" data-srcset="https://d3oyac7tbvw6tq.cloudfront.net/2015/05/user_experience-1600x800.jpg 1w, https://d3oyac7tbvw6tq.cloudfront.net/2015/05/user_experience-2000x1000.jpg 2w">
            <div class="summary"><p>We pride ourselves on thorough planning projects during our comprehensive discovery phase. </p>
                <p>Developing user stories, personas and carrying out user testing to fully encapsulate and test requirements results in a considered, appropriate and successful end product.</p>
                <p>We clearly communicate user journeys through interactive wireframes, prototypes and other tools. In addition we also carefully consider the experience for ongoing content management from a client workflow perspective, streamlining the administration processes. </p>
            </div>
        </div>
        <div class="r-tabs-accordion-title"><a href="#tab-ui" class="r-tabs-anchor">Interface<br> &amp; Interaction Design</a></div><div id="tab-ui" class="tab-content r-tabs-state-default r-tabs-panel">
            <img src="https://d3oyac7tbvw6tq.cloudfront.net/2015/05/interface-design-400x200.jpg" class="lazyload" data-srcset="https://d3oyac7tbvw6tq.cloudfront.net/2015/05/interface-design-1600x800.jpg 1w, https://d3oyac7tbvw6tq.cloudfront.net/2015/05/interface-design-2000x1000.jpg 2w">
            <div class="summary"><p>Creating clear, eye-catching and engaging interfaces has always been at the forefront of Substrakt’s vision. We keep abreast of modern web design techniques to ensure we’re making best use of emerging interaction design.</p>
                <p>Communicating our rationale on interaction and design through mockups and prototype examples enables clients to be fully involved in the process.</p>
            </div>
        </div>
        <div class="r-tabs-accordion-title"><a href="#tab-apps" class="r-tabs-anchor">Mobile &amp; Large format<br> Applications</a></div><div id="tab-apps" class="tab-content r-tabs-state-default r-tabs-panel">
            <img src="https://d3oyac7tbvw6tq.cloudfront.net/2015/05/apps-400x200.jpg" class="lazyload" data-srcset="https://d3oyac7tbvw6tq.cloudfront.net/2015/05/apps-1600x800.jpg 1w, https://d3oyac7tbvw6tq.cloudfront.net/2015/05/apps-2000x1000.jpg 2w">
            <div class="summary"><p>We build native applications for iPhone, iPad and Android devices using Objective C and Java. </p>
                <p>We also develop for mobile using web techniques that work effectively for cross platform applications.</p>
                <p>We also like to experiment with other technologies and have recently created applications with touch tables, Kinect gesture control and iBeacons.</p>
            </div>
        </div>
        <div class="r-tabs-accordion-title"><a href="#tab-strategy" class="r-tabs-anchor">Strategy &amp;<br>Ongoing Support</a></div><div id="tab-strategy" class="tab-content r-tabs-state-default r-tabs-panel">
            <img src="https://d3oyac7tbvw6tq.cloudfront.net/2015/09/Photo-12-08-2015-13-52-45-400x200.jpg" class="lazyload" data-srcset="https://d3oyac7tbvw6tq.cloudfront.net/2015/09/Photo-12-08-2015-13-52-45-1600x800.jpg 1w, https://d3oyac7tbvw6tq.cloudfront.net/2015/09/Photo-12-08-2015-13-52-45-2000x1000.jpg 2w">
            <div class="summary"><p>Beyond creating great websites and applications we consult and help implement ongoing strategies.</p>
                <p>We always aim to continue relationships with clients, working in partnership to support on an effective and productive ongoing basis.</p>
                <p>Advising on and implementing best use of digital marketing tools and techniques also helps us ensure our clients can reach their target audiences as well as clearly monitoring performance.</p>
            </div>
        </div>

    </section> <!-- skills -->


    <section class="working-life-cta cta-section wrap">
        <p>What is it like to work?</p>
        <a class="button button--sacnite" href="https://substrakt.com/working-life">Find out.</a>
    </section>

    <section class="photoshoot">
        <section class="two_up_large_right layout-row grid grid--full">
            <div class="grid__item lap-and-up-two-fifths">
                <figure class="img">
                    <img src="https://d3oyac7tbvw6tq.cloudfront.net/2015/05/IMG_7482-800x800.jpg" class=" lazyloaded" data-src="https://d3oyac7tbvw6tq.cloudfront.net/2015/05/IMG_7482-800x800.jpg">
                </figure>
            </div><div class="grid__item lap-and-up-three-fifths">
                <figure class="img">
                    <img src="https://d3oyac7tbvw6tq.cloudfront.net/2015/05/duk-workshop1-1200x800.jpg" class=" lazyloaded" data-src="https://d3oyac7tbvw6tq.cloudfront.net/2015/05/duk-workshop1-1200x800.jpg">
                </figure>
            </div>
        </section>

        <section class="two_up_large_left layout-row grid grid--full">
            <div class="grid__item lap-and-up-three-fifths">
                <figure class="img">
                    <img src="https://d3oyac7tbvw6tq.cloudfront.net/2015/05/IMG_7732-1200x800.jpg" class=" lazyloaded" data-src="https://d3oyac7tbvw6tq.cloudfront.net/2015/05/IMG_7732-1200x800.jpg">
                </figure>
            </div><div class="grid__item lap-and-up-two-fifths">
                <figure class="img">
                    <img src="https://d3oyac7tbvw6tq.cloudfront.net/2015/05/duk-workshop3-800x800.jpg" class=" lazyloaded" data-src="https://d3oyac7tbvw6tq.cloudfront.net/2015/05/duk-workshop3-800x800.jpg">
                </figure>
            </div>
        </section>
    </section>

    <section>
        <iframe style="width: 100%" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2605.4809646370227!2d28.39421958456575!3d49.2293697749189!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8bae0fbd1b98ac03!2z0IbRgiDQkNC60LDQtNC10LzRltGPINCG0L3RgtGW0YLQsA!5e0!3m2!1suk!2sua!4v1537612511939" width="1000" height="400" frameborder="0" style="border:0" allowfullscreen>

        </iframe>



<script type="text/javascript" src="https://d2i0yg27fu4v3x.cloudfront.net/wp-content/themes/substrakt/assets/js/jquery-2.1.3.min.js?ver=2.1.3"></script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=146984665735102&amp;ev=PageView&amp;noscript=1"></noscript>


<script type="text/javascript" src="https://d2i0yg27fu4v3x.cloudfront.net/wp-content/themes/substrakt/assets/js/main.js?ver=5ba6189337719"></script>
<script type="text/javascript" src="https://substrakt.com/wp-includes/js/wp-embed.min.js?ver=4.7.2"></script>


<div style="display: none; visibility: hidden;">

    <script>
//        var myLazyLoad = new LazyLoad({
//            elements_selector: ".lazy"
//        });
    </script>

    <script>(function(a,c,e,f,d,b){a.hj=a.hj||function(){(a.hj.q=a.hj.q||[]).push(arguments)};a._hjSettings={hjid:552401,hjsv:5};d=c.getElementsByTagName("head")[0];b=c.createElement("script");b.async=1;b.src=e+a._hjSettings.hjid+f+a._hjSettings.hjsv;d.appendChild(b)})(window,document,"//static.hotjar.com/c/hotjar-",".js?sv\x3d");</script></div><iframe name="_hjRemoteVarsFrame" title="_hjRemoteVarsFrame" id="_hjRemoteVarsFrame" style="display: none !important; width: 1px !important; height: 1px !important; opacity: 0 !important; pointer-events: none !important;" src="https://vars.hotjar.com/rcj-da10bd4908deb9e19dfde013ec3fe4ff.html"></iframe><div id="ads"></div></body></html>
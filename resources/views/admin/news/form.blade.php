<div class="row mB-40">
	<div class="col-sm-12">
		<div class="bgc-white p-20 bd">

			{!! Form::myInput('text', 'title', 'Title') !!}

			<textarea class="form-control" id="description" name="description">
				@if(isset($data->description))
				{!!$data->description!!}
				@endif
			</textarea>
			{!! Form::myInput('date', 'publication_time', 'Publication time') !!}

			{!! Form::myInput('boolean', 'publish', 'Published') !!}
			{{Form::file('image')}}
		</div>
	</div>
</div>


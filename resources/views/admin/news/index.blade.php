@extends('admin.default')

@section('page-header')
    News <small>{{ trans('app.manage') }}</small>
@endsection

@section('content')

    <div class="mB-20">
        <a href="{{ route(ADMIN . '.news.create') }}" class="btn btn-info">
            {{ trans('app.add_button') }}
        </a>
    </div>


    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Publication time</th>
                    <th>Published</th>
                    <th>Action</th>
                </tr>
            </thead>
            
            <tfoot>
                <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Publication time</th>
                    <th>Published</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            
            <tbody>
                @foreach ($data as $news)
                    <tr>
                        <td>{!! $news->title !!}</td>
                        <td>{!!$news->description!!}</td>
                        <td>{{ $news->publication_time }}</td>
                        @if($news->publish)
                        <td class="text-center">{{ 'already' }}</td>
                        @else
                            <td class="text-center">{{ 'not yet' }}</td>
                        @endif
                        <td>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a href="{{ route(ADMIN . '.news.edit', $news->id) }}" title="{{ trans('app.edit_title') }}" class="btn btn-primary btn-sm"><span class="ti-pencil"></span></a></li>

                                <li class="list-inline-item">
                                    <a href="{{ route(ADMIN . '.news.show', $news->id) }}" title="{{ trans('app.show_title') }}" class="btn btn-primary btn-sm">
                                        <span class="ti-ruler"></span>
                                    </a>
                                </li>

                                <li class="list-inline-item">
                                    {!! Form::open([
                                        'class'=>'delete',
                                        'url'  => route(ADMIN . '.news.destroy', $news->id),
                                        'method' => 'DELETE',
                                        ])
                                    !!}

                                    <button class="btn btn-danger btn-sm" title="{{ trans('app.delete_title') }}"><i class="ti-trash"></i></button>

                                    {!! Form::close() !!}
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        
        </table>
    </div>

@endsection
@extends('admin.default')

@section('page-header')
	News <small>{{ trans('app.update_item') }}</small>
@stop

@section('content')
	{!! Form::model($data, [
			'action' => ['NewsController@update', $data->id],
			'method' => 'put', 
			'files' => true
		])
	!!}
		@include('admin.news.form')
		<button type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button>
	{!! Form::close() !!}

@stop

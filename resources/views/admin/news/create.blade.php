@extends('admin.default')

@section('page-header')
	News <small>{{ trans('app.add_new_item') }}</small>
@stop

@section('content')
	{!! Form::open([
			'action' => ['NewsController@store'],
			'files' => true
		])
	!!}

		@include('admin.news.form')

		<button type="submit" class="btn btn-primary">{{ trans('app.add_button') }}</button>
		
	{!! Form::close() !!}
	
@stop

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Test Project') }}</title>

  {{--scripts for scrolling--}}
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TweenMax.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/scrollify/1.0.17/jquery.scrollify.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/vanilla-lazyload/10.17.0/lazyload.min.js"></script>
  <script src="{{ asset('/js/my_js.js') }}"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.0.5/vue.min.js"></script>
  <script src="{{ asset('js/app.js') }}"></script>

{{--scripts for scrolling--}}


  <!-- Styles -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.min.css">
  <link rel="stylesheet" href="  https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.4.1/css/mdb.min.css">
  <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
  <link href="{{ asset('/css/slider_css.css') }}" rel="stylesheet">
  <link href="{{ asset('/css/footer_css.css') }}" rel="stylesheet">
  <!-- Styles -->
  <style type="text/css">
    img.wp-smiley,
    img.emoji {
      display: inline !important;
      border: none !important;
      box-shadow: none !important;
      height: 1em !important;
      width: 1em !important;
      margin: 0 .07em !important;
      vertical-align: -0.1em !important;
      background: none !important;
      padding: 0 !important;
    }
  </style>
  <link rel="stylesheet" id="wp-core-css" href="https://d2i0yg27fu4v3x.cloudfront.net/wp-content/themes/substrakt/style.css?ver=4.7.2" type="text/css" media="all">
  <link rel="stylesheet" id="theme-main-css" href="https://d2i0yg27fu4v3x.cloudfront.net/wp-content/themes/substrakt/assets/css/main.css?ver=5ba618933777d" type="text/css" media="all">
  <link rel="stylesheet" id="theme-ie-adjustments-css" href="https://d2i0yg27fu4v3x.cloudfront.net/wp-content/themes/substrakt/assets/css/ie.css?ver=4.7.2" type="text/css" media="all">
  <link rel="https://api.w.org/" href="https://substrakt.com/wp-json/">
  <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://substrakt.com/xmlrpc.php?rsd">
  <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://substrakt.com/wp-includes/wlwmanifest.xml">
  <link rel="canonical" href="https://substrakt.com/about-us/">
  <link rel="shortlink" href="https://substrakt.com/?p=4604">
  <link rel="alternate" type="application/json+oembed" href="https://substrakt.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fsubstrakt.com%2Fabout-us%2F">
  <link rel="alternate" type="text/xml+oembed" href="https://substrakt.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fsubstrakt.com%2Fabout-us%2F&amp;format=xml">
  <link rel="icon" type="image/x-icon" href="https://d2i0yg27fu4v3x.cloudfront.net/wp-content/themes/substrakt/favicon.ico">
  <link href="//fonts.googleapis.com/css?family=PT+Mono" rel="stylesheet" type="text/css">
  <link type="text/css" rel="stylesheet" href="//fast.fonts.net/cssapi/7908de59-bc35-4f9f-9acd-a619cb483e7e.css">

  <style type="text/css">iframe#_hjRemoteVarsFrame {display: none !important; width: 1px !important; height: 1px !important; opacity: 0 !important; pointer-events: none !important;}</style>

</head>
<body class="app" style="background-color: #d3d3d3">
@include('blocks.navbar')
</body>

</html>

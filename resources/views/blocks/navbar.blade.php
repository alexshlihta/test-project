
<nav class="navbar navbar-expand-lg navbar-dark teal fixed-top mb-4">
    <a class="navbar-brand" href="/home">Test Project</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/home">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/all/news">News</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="/admin">Admin</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="/aboutus">About Us</a>
            </li>

        </ul>

    </div>
</nav>

<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\News::class, function (Faker $faker) {
    return [
        'title' => $faker->country,
        'description' => $faker->text('1000'),
        'publish' => $faker->boolean(),
        'publication_time' => $faker->dateTime,
    ];
});

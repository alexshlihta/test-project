<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
class PageController extends Controller
{
    public function index(Request $request)
    {
        $data = News::all();
//        if($request->ajax()){
//            return $data;
//        }
        return view('pages.news', compact('data'));
    }
    public function getNews()
    {
        $data = News::all();
//        if($request->ajax()){
//            return $data;
//        }
       return $data;
    }
    
    public function about()
    {
        return view('pages.about');
    }
    
    public function home()
    {
        return view('pages.main');
    }
    
    public function test()
    {
        $data = News::paginate(4);
        return view('pages.test', compact('data'));
    }
    
}

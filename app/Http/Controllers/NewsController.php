<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
class NewsController extends Controller
{
    public function index()
    {
        $data = News::all();
//        return $data;
        
        return view('admin.news.index', compact('data'));
    }
    
    public function show($id)
    {
        $data = News::findOrFail($id);
        return view('admin.news.show', compact('data'));
    }
    
    
        public function create()
    {
        return view('admin.news.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'title'  => 'required | min:10 | max:50',
            'description'  => 'required | max:1000',
            'publication_time'  => 'required',
            'publish'  => 'required | boolean',
        ]);
        if($validator->fails())
        {
            return $validator->errors()->all();
        }
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $photoName = time().'.'.$request->image->getClientOriginalExtension();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(200, 200);
            $image_resize->save(public_path('images/news/' .$photoName));

        }
        $news = new News();
        $news->title = $request->title;
        $news->description = $request->description;
        $news->publication_time = $request->publication_time;
        $news->publish = $request->publish;
        $news->photoname = $photoName;
        $news->save();
        return redirect()->route(ADMIN . '.news.index')->withSuccess(trans('app.success_create'));
    }

    
        public function edit($id)
    {
        $data = News::findOrFail($id);;
        return view('admin.news.edit', compact('data'));
    }
    
        public function update(Request $request, $id)
    {
        $data = News::findOrFail($id);
        $request->input('description');
        $validator = Validator::make($request->all(),[
            'title'  => 'required | min:10 | max:50',
            'description'  => 'required | max:1000',
            'publication_time'  => 'required',
            'publish'  => 'required | boolean',
        ]);
        if($validator->fails())
        {
            return $validator->errors()->all();
        }
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $photoName = time().'.'.$request->image->getClientOriginalExtension();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(200, 200);
            $image_resize->save(public_path('images/news/' .$photoName));

        }
        $data->title = $request->title;
        $data->description = $request->description;
        $data->publication_time = $request->publication_time;
        $data->publish = $request->publish;
        $data->photoname = $photoName;
        $data->save();

        return redirect()->route(ADMIN . '.news.index')->withSuccess(trans('app.success_update'));
    }

    public function destroy($id)
    {
        News::destroy($id);
        return back()->withSuccess(trans('app.success_destroy'));
    }
}
